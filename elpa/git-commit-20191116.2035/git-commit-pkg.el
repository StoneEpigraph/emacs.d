;;; -*- no-byte-compile: t -*-
(define-package "git-commit" "20191116.2035" "Edit Git commit messages" '((emacs "25.1") (dash "20180910") (with-editor "20181103")) :commit "4b97f23ace4132b3313a1bca73dcbbcd06bb4b9b" :keywords '("git" "tools" "vc") :maintainer '("Jonas Bernoulli" . "jonas@bernoul.li") :url "https://github.com/magit/magit")
