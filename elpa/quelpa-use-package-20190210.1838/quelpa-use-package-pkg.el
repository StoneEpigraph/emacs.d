;;; -*- no-byte-compile: t -*-
(define-package "quelpa-use-package" "20190210.1838" "quelpa handler for use-package" '((emacs "24.3") (quelpa "0") (use-package "2")) :commit "207c285966382a1f33681f6ac2d7778d4b21cb21" :keywords '("package" "management" "elpa" "use-package") :authors '(("steckerhalter")) :maintainer '("steckerhalter") :url "https://framagit.org/steckerhalter/quelpa-use-package")
