;;; counsel-dash-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "counsel-dash" "counsel-dash.el" (24056 58031
;;;;;;  285300 78000))
;;; Generated autoloads from counsel-dash.el

(autoload 'counsel-dash "counsel-dash" "\
Query dash docsets.
INITIAL will be used as the initial input, if given.

\(fn &optional INITIAL)" t nil)

(autoload 'counsel-dash-at-point "counsel-dash" "\
Bring up a `counsel-dash' search interface with symbol at point.

\(fn)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; counsel-dash-autoloads.el ends here
