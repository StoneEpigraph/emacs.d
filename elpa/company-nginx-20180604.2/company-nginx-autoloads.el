;;; company-nginx-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "company-nginx" "company-nginx.el" (24163 7566
;;;;;;  548143 787000))
;;; Generated autoloads from company-nginx.el

(autoload 'company-nginx-keywords "company-nginx" "\
Add Nginx directive keywords to company-keywords-alist.

\(fn)" nil nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; company-nginx-autoloads.el ends here
