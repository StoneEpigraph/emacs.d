;;; -*- no-byte-compile: t -*-
(define-package "ppp" "20200119.1929" "Extended pretty printer for Emacs Lisp" '((emacs "25.1")) :commit "9fdfd93cad2f58cea735254602c9139f1baf8a5e" :keywords '("tools") :authors '(("Naoya Yamashita" . "conao3@gmail.com")) :maintainer '("Naoya Yamashita" . "conao3@gmail.com") :url "https://github.com/conao3/ppp.el")
