;;; ppp-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "ppp" "ppp.el" (24101 22360 581540 400000))
;;; Generated autoloads from ppp.el

(autoload 'ppp-sexp-to-string "ppp" "\
Output the pretty-printed representation of FORM suitable for objects.
See `ppp-sexp' to get more info.

\(fn FORM)" nil t)

(autoload 'ppp-macroexpand-to-string "ppp" "\
Output the pretty-printed representation of FORM suitable for macro.
See `ppp-macroexpand' to get more info.

\(fn FORM)" nil t)

(autoload 'ppp-macroexpand-all-to-string "ppp" "\
Output the pretty-printed representation of FORM suitable for macro.
Unlike `ppp-macroexpand', use `macroexpand-all' instead of `macroexpand-1'.
See `ppp-macroexpand-all' to get more info.

\(fn FORM)" nil t)

(autoload 'ppp-list-to-string "ppp" "\
Output the pretty-printed representation of FORM suitable for list.
See `ppp-list' to get more info.

\(fn FORM)" nil t)

(autoload 'ppp-plist-to-string "ppp" "\
Output the pretty-printed representation of FORM suitable for plist.
See `ppp-plist' to get more info.

\(fn FORM)" nil t)

(autoload 'ppp-sexp "ppp" "\
Output the pretty-printed representation of FORM suitable for objects.

\(fn FORM)" nil nil)

(autoload 'ppp-macroexpand "ppp" "\
Output the pretty-printed representation of FORM suitable for macro.

\(fn FORM)" nil t)

(autoload 'ppp-macroexpand-all "ppp" "\
Output the pretty-printed representation of FORM suitable for macro.
Unlike `ppp-macroexpand', use `macroexpand-all' instead of `macroexpand-1'.

\(fn FORM)" nil t)

(autoload 'ppp-list "ppp" "\
Output the pretty-printed representation of FORM suitable for list.

\(fn FORM)" nil nil)

(autoload 'ppp-plist "ppp" "\
Output the pretty-printed representation of FORM suitable for plist.

\(fn FORM)" nil nil)

(autoload 'ppp-debug "ppp" "\
Output debug message to `flylint-debug-buffer'.

ARGS accepts (KEYWORD-ARGUMENTS... PKG FORMAT &rest FORMAT-ARGS).

Auto arguments:
  PKG is symbol.
  FORMAT and FORMAT-ARGS passed `format'.

Keyword arguments:
  If LEVEL is specified, output higher than
  `ppp-minimum-warning-level--{{PKG}}' initialized `ppp-minimum-warning-level'.
  LEVEL should be one of :debug, :warning, :error, or :emergency.
  If LEVEL is omitted, assume :debug.
  If BUFFER is specified, output that buffer.
  If POPUP is non-nil, `display-buffer' debug buffer.
  If BREAK is non-nil, output page break before output string.

Note:
  If use keyword arguments, must specified these before auto arguments.

\(fn &key buffer level break PKG FORMAT &rest FORMAT-ARGS)" nil t)

(function-put 'ppp-debug 'lisp-indent-function 'defun)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; ppp-autoloads.el ends here
